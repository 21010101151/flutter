import 'dart:math';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
int i=1;
int j=2;
int k=3;



class Dice extends StatefulWidget {
  const Dice({Key? key}) : super(key: key);

  @override
  State<Dice> createState() => _DiceState();
}

class _DiceState extends State<Dice> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      backgroundColor: Colors.cyan,
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(child: InkWell(
                onTap: (){
                  getRandom();
                },
                child: Container(padding: EdgeInsets.all(10),margin: EdgeInsets.all(8),child: Image(image: AssetImage("assets/dice$i.png"))))),
            Expanded(child:InkWell(
                onTap: (){
                  getRandom();
                }
                ,child: Container(padding: EdgeInsets.all(10),margin: EdgeInsets.all(8),child: Image(image: AssetImage("assets/dice$j.png"))))),
            Expanded(child:InkWell(
                onTap: (){
                  getRandom();
                }
                ,child: Container(padding: EdgeInsets.all(10),margin: EdgeInsets.all(8),child: Image(image: AssetImage("assets/dice$k.png"))))),

          ],
        ),

      ),
    );

  }
  void getRandom(){
    setState((){
      i=Random().nextInt(5)+1;
      j=Random().nextInt(5)+1;
      k=Random().nextInt(5)+1;

    });
  }
}


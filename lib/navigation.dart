import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'First.dart';
import 'image.dart';

class Costomnavigation extends StatefulWidget {
  const Costomnavigation({Key? key}) : super(key: key);

  @override
  State<Costomnavigation> createState() => _CostomnavigationState();
}

class _CostomnavigationState extends State<Costomnavigation> {
  @override
  void initState() {

    super.initState();
    print('step-1');
  }

  @override
  void didChangeDependencies() {

    super.didChangeDependencies();
    print('step-3');
  }
  @override
  void didUpdateWidget(covariant Costomnavigation oldWidget) {

    super.didUpdateWidget(oldWidget);
    print('step-4');
  }
  @override
  void deactivate() {

    super.deactivate();
    print('step--5');
  }
  @override
  void dispose() {
    super.dispose();
    print('step-6');
  }


  int selectedIdex=0;

  List<Widget> widgetList =[
    Dice(),
    Imag(),
    Container(color: Colors.grey)
  ];
  @override
  Widget build(BuildContext context) {
    print('step-2');
    return  Scaffold(
      body: widgetList[selectedIdex],
      bottomNavigationBar: BottomNavigationBar(items : [
        BottomNavigationBarItem(
          icon: Icon(Icons.dashboard_outlined),
          label: 'Dice,'
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.image),
            label: 'Image',
          backgroundColor: Colors.cyan
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.hd_outlined),
            label: 'Color',
            backgroundColor: Colors.cyan
        ),

      ],
        backgroundColor: Colors.deepOrange,
        onTap: (index){
        setState((){
          selectedIdex = index;
        },
        );

        },

      ),


    );
  }
}

// appBar: AppBar(
//     title:Text('Select'),
//
//   backgroundColor: Colors.black,
//     leading: IconButton(
//       icon: Icon(
//         Icons.share,
//         color: Colors.white,
//       ),
//       onPressed: () {
//         // do something
//       },),
//
// ),
// Stack(
//   children: [
//     Container(
//       child: Image.asset('assets/background.jpg',fit: BoxFit.cover,),
//       width: double.infinity,
//       height: double.infinity,
//     ),
//   ],
// ),

// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
//
// class Home extends StatelessWidget {
//   const Home({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body:
//       Container(
//
//         child: Stack(
//           children: [
//             Container(
//               child: Image.asset('assets/background.jpg', fit: BoxFit.cover,),
//               width: double.infinity,
//               height: double.infinity,
//             ),
//             Padding(padding: const EdgeInsets.all(30.0),
//               child:
//               Column(
//                 children: [
//                   Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     children: [
//                       Container(
//                         decoration: BoxDecoration(
//                           color: Colors.black,
//
//
//                         ),
//                         padding: EdgeInsets.all(16),
//                         child: Icon(Icons.share, color: Colors.white,),
//                       ),
//
//
//                       Column(
//                         children: [
//                           Text('Select', style: TextStyle(
//                               color: Colors.white,
//                               fontSize: 24,
//                               fontWeight: FontWeight.w100
//                           ),
//                           ),
//                           SizedBox(height: 8,),
//                           Text('Car', style: TextStyle(
//                               color: Colors.white,
//                               fontSize: 26,
//                               fontWeight: FontWeight.w100),),
//                         ],
//
//                       ),
//                       Column(
//                         mainAxisAlignment: MainAxisAlignment.end,
//                         children: [
//                           CircleAvatar(
//                             backgroundImage: AssetImage('assets/dice1.png'),
//                           )
//                         ],
//                       ),
//
//
//                     ],
//                   ),
//                 ],
//               ),
//
//             ),
//             Column(
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: [
//                 Container(
//                   decoration: BoxDecoration(
//                     color: Colors.black,
//                     borderRadius: BorderRadius.only(
//                       topRight: Radius.circular(40),
//                       topLeft: Radius.circular(40),
//                     ),
//                   ),
//
//                 ),
//                 Text('From',style: TextStyle(
//                     color: Colors.white
//                 ),),
//               ],
//             ),
//             Text('to:',style: TextStyle( color: Colors.white),)
//
//
//           ],
//         ),
//       ),
//
//
//     );
//   }
// }
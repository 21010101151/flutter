import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Screen2.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Container(
          child: Stack(
            children: [
              Container(
                child: Image.asset(
                  'assets/map.png',
                  fit: BoxFit.cover,
                ),
                width: double.infinity,
                height: double.infinity,
              ),
              Column(
                children: [
                  Expanded(
                    flex: 3,
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.black87,
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(40),
                          bottomLeft: Radius.circular(40),
                        ),
                      ),
                      child: Column(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(40),
                                bottomLeft: Radius.circular(40),
                              ),
                            ),
                            child:
                            Padding(
                              padding: const EdgeInsets.all(50.0),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Container(
                                            decoration: BoxDecoration(
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Colors.white,
                                                    blurRadius: 7,
                                                    spreadRadius: 1,
                                                  )
                                                ],
                                                color: Colors.black,
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(15),
                                                )),
                                            padding: EdgeInsets.all(16),
                                            child: Icon(Icons.share,
                                                color: Colors.white, size: 7),
                                          ),
                                          Column(
                                            children: [
                                              Container(
                                                margin: EdgeInsets.only(left: 30),
                                                child:
                                                Text(
                                                  'Select',
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 24,
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: Text(
                                                  'Car',
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 26,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                      CircleAvatar(
                                        backgroundImage:
                                            AssetImage('assets/Profile.jpg'),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(

                            child: Container(
                              height: 100,
                              child: Column(children: [
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        margin: EdgeInsets.only(left: 40),
                                        child: Text(
                                          'From :-',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 20,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Container(
                                        child: Text(
                                          'To :-',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 20,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(left: 33),
                                      decoration: BoxDecoration(
                                          color: Colors.grey,
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(15),
                                          )),
                                      padding: EdgeInsets.all(7),
                                      child: Icon(Icons.circle,
                                          color: Colors.white, size: 12),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 10),
                                      child: Text(
                                        'My Location',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20,

                                          // fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 25),
                                      decoration: BoxDecoration(
                                          color: Colors.yellow,
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(15),
                                          )),
                                      padding: EdgeInsets.all(5),
                                      child: Icon(Icons.location_on,
                                          color: Colors.black, size: 20),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 10),
                                      child: Text(
                                        'Librity St',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20,

                                          // fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    )
                                  ],
                                )
                              ]),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(),
                  ),
                  Expanded(
                      flex: 4,
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.only(bottom: 10),
                            height: 330,
                            child: ListView(
                              scrollDirection: Axis.horizontal,
                              children: [
                                Container(
                                  width: 200,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(25),
                                    color: Colors.white,
                                  ),
                                  margin: EdgeInsets.only(left: 40),
                                  child: Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: Stack(
                                      alignment: Alignment.bottomCenter,
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(25),
                                          child: Image.asset(
                                            "assets/verna2022.jpg",
                                            fit: BoxFit.fill,
                                            width: 200,
                                            height: 400,
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.all(5),
                                          height: 89,
                                          width: 190,
                                          decoration: BoxDecoration(
                                            color: Color(0xAAFFFFFF),
                                            borderRadius:
                                                BorderRadius.circular(20),
                                          ),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Expanded(
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(25),
                                                  child: Container(
                                                    alignment: Alignment.center,
                                                    width: 45,
                                                    height: 25,
                                                    color: Colors.grey,
                                                    child: Text(
                                                      "\$29",
                                                      style: TextStyle(
                                                          fontSize: 15),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      left: 10, top: 7),
                                                  child: Text(
                                                    "\$verna 2022",
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: Row(
                                                  children: [
                                                    Expanded(
                                                        child: Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 7,
                                                                    bottom: 7),
                                                            child: Row(
                                                              children: [
                                                                Text(
                                                                    "4 Person Can Ride"),
                                                              ],
                                                            ))),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.of(context)
                                        .push(MaterialPageRoute(
                                      builder: (context) {
                                        return Screen2();
                                      },
                                    ));
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(25),
                                      color: Colors.white,
                                    ),
                                    margin: EdgeInsets.only(left: 20),
                                    child: Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: Stack(
                                        alignment: Alignment.bottomCenter,
                                        children: [
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(25),
                                            child: Image.asset(
                                              "assets/verna.jpg",
                                              fit: BoxFit.fill,
                                              width: 200,
                                              height: 400,
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.all(5),
                                            height: 89,
                                            width: 190,
                                            decoration: BoxDecoration(
                                              color: Color(0xAAFFFFFF),
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                            ),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Expanded(
                                                  child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            25),
                                                    child: Container(
                                                      alignment:
                                                          Alignment.center,
                                                      width: 45,
                                                      height: 25,
                                                      color: Colors.grey,
                                                      child: Text(
                                                        "\$29",
                                                        style: TextStyle(
                                                            fontSize: 15),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                  child: Container(
                                                    margin: EdgeInsets.only(
                                                        left: 10, top: 7),
                                                    child: Text(
                                                      "\$verna 2023",
                                                      style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                  child: Row(
                                                    children: [
                                                      Expanded(
                                                          child: Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      left: 7,
                                                                      bottom:
                                                                          7),
                                                              child: Row(
                                                                children: [
                                                                  Text(
                                                                      "4 Person Can Ride"),
                                                                ],
                                                              ))),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(25),
                                    color: Colors.white,
                                  ),
                                  margin: EdgeInsets.only(left: 20),
                                  child: Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: Stack(
                                      alignment: Alignment.bottomCenter,
                                      children: [
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(25),
                                          child: Image.asset(
                                            "assets/audis5.jpg",
                                            fit: BoxFit.fill,
                                            width: 230,
                                            height: 400,
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.all(5),
                                          height: 89,
                                          width: 190,
                                          decoration: BoxDecoration(
                                            color: Color(0xAAFFFFFF),
                                            borderRadius:
                                                BorderRadius.circular(20),
                                          ),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Expanded(
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(25),
                                                  child: Container(
                                                    alignment: Alignment.center,
                                                    width: 45,
                                                    height: 25,
                                                    color: Colors.grey,
                                                    child: Text(
                                                      "\$29",
                                                      style: TextStyle(
                                                          fontSize: 15),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: Container(
                                                  margin: EdgeInsets.only(
                                                      left: 10, top: 7),
                                                  child: Text(
                                                    "\$audiS5",
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: Row(
                                                  children: [
                                                    Expanded(
                                                        child: Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 7,
                                                                    bottom: 7),
                                                            child: Row(
                                                              children: [
                                                                Text(
                                                                    "4 Person Can Ride"),
                                                              ],
                                                            ))),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      )),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

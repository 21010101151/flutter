import 'package:flutter/material.dart';
import 'package:futeer_project/First.dart';
import 'package:futeer_project/Screen2.dart';
import 'package:futeer_project/Screen3.dart';
import 'package:futeer_project/text.dart';
import 'firstpage.dart';
import 'fpage.dart';
import 'image.dart';
import 'navigation.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      home: Home(),
    );
  }
}
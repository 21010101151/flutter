
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Screen2.dart';
import 'firstpage.dart';

class Screen3 extends StatefulWidget {
  const Screen3({Key? key}) : super(key: key);

  @override
  State<Screen3> createState() => _Screen3State();
}

class _Screen3State extends State<Screen3> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.black,
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(50.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment:
                    MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.white,
                                    blurRadius: 7,
                                    spreadRadius: 1,
                                  )
                                ],
                                color: Colors.black,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(15),
                                )),
                            padding: EdgeInsets.all(10),
                            child: Icon(Icons.share,
                                color: Colors.white, size: 17),
                          ),
                          Column(
                            children: [
                              Container(
                                margin: EdgeInsets.only(left: 30),
                                child: Text(
                                  'Touch and',
                                  style: TextStyle(
                                    color: Colors.white54,
                                    fontSize: 24,

                                  ),
                                ),
                              ),
                              Container(
                                child: Text(
                                  'Pay',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 26,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),

                    ],
                  ),
                ],
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(500),
                      bottomRight: Radius.circular(40),
                  )
              ),
              child: Stack(
                children: [
                  Container(



                    child: Image.asset(
                      'assets/creditcard.jpg',
                      fit: BoxFit.cover,
                    ),
                     width: 380,
                    // height: 300,
                  ),
                  Container(
                    child: Column(
                      children: [
                      Container(
                        margin: EdgeInsets.only(top: 30,right: 230),

                         child: Text('uPay',style: TextStyle(
                      color: Colors.black,
                      fontSize: 32,
                           fontWeight: FontWeight.bold
                    ),),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 16,top: 20),

                        child: Text('2929  1920  1112  0766',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 33,
                            fontWeight: FontWeight.bold
                        ),),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 100,top: 48),

                        child: Text('Debit \n Dhrutik Rabadiya',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 26,
                             fontWeight: FontWeight.bold
                          ),),

                      ),

                    ],),
                  )

                ],
              ),
            ),
      InkWell(
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(
            builder: (context) {
              return Home();
            },
          ));
        },
      child:  Container(margin:EdgeInsets.only(top:150),
          child:
          Icon(Icons.fingerprint_rounded,color: Colors.white, size: 150)
      ),)

          ],
        ) ,
      ),
    );
  }
}
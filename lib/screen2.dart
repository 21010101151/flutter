import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:futeer_project/Screen3.dart';

class Screen2 extends StatefulWidget {
  const Screen2({Key? key}) : super(key: key);

  @override
  State<Screen2> createState() => _Screen2State();
}

class _Screen2State extends State<Screen2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
      Container(
        child: Stack(

          children: [
            Container(
              child: Image.asset('assets/verna2023.jpg',fit: BoxFit.cover,),
              width: double.infinity,
              height: double.infinity,
            ),
            Column(
              children: [
                Container(
                  child: Padding(padding: const EdgeInsets.all(40.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.white54,
                                  blurRadius: 4,
                                  spreadRadius: 1,
                                )
                              ],
                              color: Colors.black,
                              borderRadius: BorderRadius.all(Radius.circular(15),)
                          ),
                          padding: EdgeInsets.all(16),
                          child: Icon(Icons.share,color: Colors.white,size: 15),
                        ),
                        Container(
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.white54,
                                  blurRadius: 4,
                                  spreadRadius: 1,
                                )
                              ],
                              color: Colors.black,
                              borderRadius: BorderRadius.all(Radius.circular(15),)
                          ),
                          padding: EdgeInsets.all(16),
                          child: Icon(Icons.settings_input_composite_outlined,color: Colors.white,size: 15),
                        ),
                      ],
                    ),
                  ),
                ),
                Column(
                  children: [
                    Container(
                      child:
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(margin: EdgeInsets.only(right: 230,top: 20),

                            decoration: BoxDecoration(
                                boxShadow: [
                                       BoxShadow(
                                             color: Colors.white54,
                                              blurRadius: 7,
                                              spreadRadius: 1,
                                          )
                                           ],

                                color: Colors.black54,
                                borderRadius: BorderRadius.all(Radius.circular(15),)
                            ),
                            padding: EdgeInsets.all(16),
                            child:
                            Column(
                              children: [
                                Container(
                                  child: Icon(Icons.power_settings_new_rounded,color: Colors.white,size: 20),
                                ),
                                Container(
                                  child: Text(
                                    '2500cc',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 22  ,
                                    ),
                                  ),
                                ),
                              ],

                            )

                          ),
                        ],
                      ),

                    ),
                    Container(margin: EdgeInsets.only(top: 50),
                      child:
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(margin: EdgeInsets.only(right: 230),

                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.white54,
                                      blurRadius: 7,
                                      spreadRadius: 1,
                                    )
                                  ],
                                  color: Colors.black54,
                                  borderRadius: BorderRadius.all(Radius.circular(15),)
                              ),
                              padding: EdgeInsets.all(16),
                              child: //Icon(Icons.share,color: Colors.white,size: 15),
                              Column(
                                children: [
                                  Container(

                                    child: Icon(Icons.gas_meter,color: Colors.white,size: 20),
                                  ),
                                  Container(
                                    child: Text(
                                      ' Petrol ',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 24,
                                      ),
                                    ),
                                  ),
                                ],

                              )

                          ),



                        ],
                      ),




                    ),
                    Container(margin: EdgeInsets.only(top: 50),
                      child:
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(margin: EdgeInsets.only(right: 230),

                              decoration: BoxDecoration(
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.white54,
                                      blurRadius: 7,
                                      spreadRadius: 1,
                                    )
                                  ],
                                  color: Colors.black54,
                                  borderRadius: BorderRadius.all(Radius.circular(15),)
                              ),
                              padding: EdgeInsets.all(16),
                              child: //Icon(Icons.share,color: Colors.white,size: 15),
                              Column(
                                children: [
                                  Container(
                                    child: Icon(Icons.airline_seat_recline_extra,color: Colors.white,size: 20),
                                  ),
                                  Container(
                                    child: Text(
                                      '4 Seats',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 24,
                                      ),
                                    ),
                                  ),
                                ],

                              )

                          ),



                        ],
                      ),




                    ),

                  ],
                ),
                Container(margin: EdgeInsets.only(top: 120),
                  decoration: BoxDecoration(
                    color: Colors.black54,
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(50),
                      topLeft: Radius.circular(50),
                    ),
                  ),
                  child:
                  Column(

                    children: [
                      Row(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(margin: EdgeInsets.only(left: 25,top: 20),child:
                          Text('Driver',style: TextStyle(color: Colors.white54,fontSize: 18),),),
                          Container(margin: EdgeInsets.only(top: 20,right: 10),
                            child: 
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  child: Icon(Icons.star,color: Colors.white),
                                ),
                                Container(
                                  child:
                                  Text('4.9',style:TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white

                                  ) ,),
                                )
                              ],
                            ),
                          )
                          
                        ],
                      ),
                      Row(
                        children: [
                          Container( margin: EdgeInsets.only(left: 30,top: 15),
                            child: CircleAvatar(
                              backgroundImage:
                              AssetImage('assets/Profile.jpg'),
                            ),
                          ),
                          Column(
                            children: [
                              Container(margin: EdgeInsets.only(left: 10,top: 15),
                                child:
                                Text('Darsh Dobariya',style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20
                                ),),
                              ),
                              Container(margin: EdgeInsets.only(right: 30),
                                child:
                                Text('98765 43210',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 15

                                ),),
                              )

                            ],
                          ),
                          Container( margin: EdgeInsets.only(left: 150,top: 15),
                            child:  Icon(Icons.checklist_rtl_sharp,color: Colors.white,size: 18),
                          )

                        ],
                      ),
                  InkWell(
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(
                        builder: (context) {
                          return Screen3();
                        },
                      ));
                    },
                    child: Row(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.yellow,
                            borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(40),
                              bottomLeft: Radius.circular(40),
                              topRight: Radius.circular(40),
                              topLeft: Radius.circular(40),
                            ),
                          ),

                          width: 300,
                          height: 50,

                          margin: EdgeInsets.only(left: 30,top: 15),
                          child: Center(
                            child: Text('Book A Car',style: TextStyle(
                                fontSize: 20,

                            ),),
                          ),

                        ),
                        Container(margin: EdgeInsets.only(top: 15,left: 7),
                            decoration: BoxDecoration(
                                color: Colors.blueGrey,
                                borderRadius: BorderRadius.all(Radius.circular(15),)
                            ),

                            padding: EdgeInsets.all(12),

                            child: Icon(Icons.call,color: Colors.yellow,)

                        )
                      ],

                    ) ,
                  ),

                    ],
                  ),
                )


              ],
            )
          ],
        ),
      )
      ,
    );
  }
}
